//
//  UIViewExtention.swift
//  ViewExtantion
//
//  Created by User on 2019/7/8.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
struct AncoredConstraints {
    var top, leading, buttom,trailing, width, height: NSLayoutConstraint?
}


extension UIView {
    
    func fillSuperview(padding: UIEdgeInsets = .zero) {
        anchor(top: superview?.topAnchor, leading: superview?.leadingAnchor, trailing: superview?.trailingAnchor, bottom: superview?.bottomAnchor,padding: padding)
    }
    func anchorSize(to view:UIView) {
        widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    @discardableResult
    func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, trailing: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) -> AncoredConstraints {
        
        translatesAutoresizingMaskIntoConstraints = false
        var ancoredConstraints = AncoredConstraints()
        
        if let top = top {
            ancoredConstraints.top = topAnchor.constraint(equalTo: top, constant:padding.top)
        }
        if let leading = leading {
            ancoredConstraints.leading = leadingAnchor.constraint(equalTo: leading, constant: padding.left)
        }
        if let trailing = trailing {
            
            ancoredConstraints.trailing = trailingAnchor.constraint(equalTo: trailing, constant: -padding.right)
        }
        if let bottom = bottom {
            ancoredConstraints.buttom = bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom)
        }
        
        if size.height != 0 {
            ancoredConstraints.height = heightAnchor.constraint(equalToConstant: size.height)
        }
        if size.width != 0 {
            ancoredConstraints.width = widthAnchor.constraint(equalToConstant: size.width)
        }
        [ancoredConstraints.top, ancoredConstraints.leading, ancoredConstraints.buttom, ancoredConstraints.trailing, ancoredConstraints.width, ancoredConstraints.height].forEach { $0?.isActive = true }
        
        return ancoredConstraints
    }
    
    func centerInSuperview(size: CGSize = .zero) {
        translatesAutoresizingMaskIntoConstraints = false
        
        if let superviewCenterXAnchor =  superview?.centerXAnchor{
            centerXAnchor.constraint(equalTo: superviewCenterXAnchor).isActive = true
        }
        if let superviewCenterYAnchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: superviewCenterYAnchor).isActive = true
        }
        anchor(top: nil, leading: nil, trailing: nil, bottom: nil, size: size)
    }
}
